# Vote help

## Table of Content

- Introduction
- Requirements
- Installation & Configuration
- Troubleshooting
- FAQ
- Maintainer

## Introduction

The VOTE! Module is a JS-based plugin for user votings. Any content entity like
Nodes, taxonomy terms, paragraphs, media entities, custom entities, etc. may
receive customizable voting widgets. One or more individual voting types can be
attached and evaluated for each entity type and bundle.

The module enables optimal page and data caching, which is even compatible with
static page caching (e.g. Tome).

The Voting API is used as the backend, so it is relatively easy to migrate
existing voting data from other voting plugins. It is also possible to sort the
entities in views according to user voting.

## Requirements

1. Check the Extend page ```/admin/modules``` to ensure that the following
   modules are properly installed and up to date:
   1. VOTE!
   2. Voting API
2. Check the status report ```/admin/reports/status``` for error messages.
3. Set the permissions for all user roles that should participate in voting
   under: Home > Administration > People >
   Permissions ```/admin/people/permissions#module-vote```

## Installation & Configuration

1. In the admin menu go to the page: Home > Administration > Configuration >
   Web services > VOTE! Configuration: ```/admin/config/services/vote```
2. Add a new Vote Type with "Add Vote type". Use the field descriptions as a
   guide. Pay particular attention to the following points:
   1. Choose a voting widget (e.g. Five-Star or Yes-No). This setting cannot be
     changed later, especially if voting data has already been entered.
   2. Some voting widgets offer an abstention function (abstention, null value)
     (e.g. yes / no / abstention). If you have made changes to the "Voting
     Widgets" or "Abstention allowed" fields, check your entries again after
     saving, especially the "Options / Labels" field. There may have been
     changes made.
   3. Icon family: In this module, icons are provided as SVG sprites. If you
      want
     to use the default SVG sprite, leave the field blank but check if the
     following icon id is included in the default SVG sprite.
   4. After saving, check whether a corresponding voting type has been
      created in the voting API: ```/admin/structure/vote-types```
3. If you have created at least one vote type, switch to the "Settings" tab.
   Here you can set which entities should get a voting field and which voting
   types should be used.
   1. To activate voting e.g for the node-type article, select the Content tab
     under "Entity to Vote Types map" and first activate the "Content" checkbox.
   2. A new field then appears on the same tab for each node bundle, in
      which you
     can enter the voting types that are to be used for this node bundle.
   3. Multiple voting types can be entered for each entity bundle.
   4. Save the settings.
4. Now switch to the view settings of the entity bundles to which (under 3.)
   vote types have been assigned. For example, Home > Administration >
   Structure > Content types > Article > Manage display
   ```/admin/structure/types/manage/article/display```
   1. Now a new field "Vote" appears in the field list (as in all view modes of
     the same bundle). Here you can configure for each view mode of the entity
     bundle whether and how the voting widget should be displayed for the
     website users.
   2. First check whether the correct formatter is displayed for the "Vote"
      field
     in the "Format" column: "Vote plugin formatter".
   3. Open the settings dialog for the bot field by clicking on the settings
      icon
     and choose your display options.
   4. If you don't want the vote field to be displayed at all in a view mode,
     move it down the list below Disabled.
5. Clear the cache once and look at the voting widget in the frontend. -
   Finished

## Troubleshooting

1. Voting widget is not visible in Frontend.
   - Check permissions for current user.
   - Check display configuration for the entity bundle and each view-mode.
   - Check for Javascript errors in the browser developer tools.

## FAQ

1. Does the module break site caching because of user specific content?
   - No, the module loads all individual voting data by separate API calls.
2. Can I implement multiple Voting types to the same entity?
   - Yes, you can.
3. Can I customize the layout and styles?
   - You can customize the widget by CSS as you like, but you can't touch the
   markup inside widget. Markup is JSX.

## Browser compatibility

- Firefox 51+
- Chrome 54+
- Safari 14+
- Opera 38+
- Edge 78+

## Links

* [Issue Tracking](https://www.drupal.org/project/issues/vote)
* [Code Repository](https://git.drupalcode.org/project/vote)

## Maintainer

[Joachim Feltkamp](https://www.drupal.org/u/jfeltkamp), Hamburg Germany
