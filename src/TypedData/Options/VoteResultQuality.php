<?php

namespace Drupal\vote\TypedData\Options;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\OptionsProviderInterface;

/**
 * Options provider for the types of field access to check for.
 */
class VoteResultQuality implements OptionsProviderInterface {

  const QUALITY_GOOD = 'good';
  const QUALITY_BAD = 'bad';
  const QUALITY_NEUTRAL = 'neutral';

  /**
   * {@inheritdoc}
   */
  public function getPossibleOptions(AccountInterface $account = NULL): array {
    return self::getOptions();
  }

  /**
   * Returns options for selecting a vote quality.
   *
   * @return TranslatableMarkup[]
   */
  public static function getOptions (): array {
    return [
      self::QUALITY_GOOD => new TranslatableMarkup('Good'),
      self::QUALITY_NEUTRAL => new TranslatableMarkup('Neutral'),
      self::QUALITY_BAD => new TranslatableMarkup('Bad')
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleValues(AccountInterface $account = NULL): array
  {
    return array_keys($this->getPossibleOptions($account));
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableValues(AccountInterface $account = NULL): array
  {
    return $this->getPossibleValues($account);
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableOptions(AccountInterface $account = NULL): array
  {
    return $this->getPossibleValues($account);
  }

}
