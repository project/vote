<?php

namespace Drupal\vote\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class VoteVoteController manages the API calls from frontend.
 */
class VoteVoteController extends ControllerBase {

  /**
   * Drupal\vote\VotingApiService definition.
   *
   * @var \Drupal\vote\VotingApiService
   */
  protected $voteVotingapi;

  /**
   * Collection of used VoteBaseType instances.
   *
   * @var \Drupal\vote\Entity\VoteTypeInterface[]
   */
  protected $voteTypes;

  /**
   * Collection of used VoteBaseType instances.
   *
   * @var \Drupal\Core\Entity\ContentEntityTypeInterface[]
   */
  protected $entities;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->voteVotingapi = $container->get('vote.votingapi');
    return $instance;
  }

  /**
   * Set a vote on an entity.
   *
   * @param string $entity_type_id
   *   The entity type id of the voting target.
   * @param string $entity_id
   *   The entity id of the voting target.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The Symfony request object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response to the frontend.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function setVote(string $entity_type_id, string $entity_id, Request $request) {

    $entity_slug = "{$entity_type_id}:{$entity_id}";

    $content = ($request->getMethod() == 'POST')
      ? json_decode($request->getContent(), TRUE)
      : [];

    $votes = $content['votes'] ?? [];
    if (is_array($votes)) {
      $updated_results = [];
      $response = [];
      foreach ($votes as $vote_type_id => $vote_id) {
        $response[$vote_type_id] = $this->voteVotingapi
          ->setVote($entity_type_id, $entity_id, $vote_type_id, $vote_id);
      }
      $this->voteVotingapi->refresh($entity_type_id, $entity_id, array_keys($votes));
      $updated_results[$entity_slug] = $this->voteVotingapi->getResults($entity_type_id, $entity_id);

      $data = [
        'response' => $response,
        'updated_results' => $updated_results,
      ];
    }

    return new JsonResponse($data);
  }

  /**
   * Get a set of results.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Symfony request object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Symfony JSON response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getResults(Request $request) {

    $content = ($request->getMethod() == 'POST')
      ? json_decode($request->getContent(), TRUE)
      : [];

    $results = [];
    $entity_slugs = $content ?? [];
    foreach ($entity_slugs as $entity_slug) {
      $frags = explode(':', $entity_slug);
      $entity_type_id = $frags[0];
      $entity_id = $frags[1];

      $results[$entity_slug] = $this->voteVotingapi->getResults($entity_type_id, $entity_id);
    }

    return new JsonResponse($results);
  }

}
