<?php

namespace Drupal\vote\Plugin\VoteResultFunction;

use Drupal\votingapi\VoteResultFunctionBase;

/**
 * The total number of 2 points votes.
 *
 * @VoteResultFunction(
 *   id = "points_2",
 *   label = @Translation("Points 2 votes"),
 *   description = @Translation("The number of votes with 2 points."),
 *   num_value = 2
 * )
 */
class Points2 extends VoteResultFunctionBase {

  /**
   * {@inheritdoc}
   */
  public function calculateResult($votes) {
    $sum = 0;
    foreach ($votes as $vote) {
      if ($vote->getValue() == 2) {
        $sum++;
      }
    }
    return $sum;
  }

}
