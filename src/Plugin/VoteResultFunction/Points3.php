<?php

namespace Drupal\vote\Plugin\VoteResultFunction;

use Drupal\votingapi\VoteResultFunctionBase;

/**
 * The total number of 3 points votes.
 *
 * @VoteResultFunction(
 *   id = "points_3",
 *   label = @Translation("Points 3 votes"),
 *   description = @Translation("The number of votes with 3 points."),
 *   num_value = 3
 * )
 */
class Points3 extends VoteResultFunctionBase {

  /**
   * {@inheritdoc}
   */
  public function calculateResult($votes) {
    $sum = 0;
    foreach ($votes as $vote) {
      if ($vote->getValue() == 3) {
        $sum++;
      }
    }
    return $sum;
  }

}
