<?php

namespace Drupal\vote\Plugin\VoteResultFunction;

use Drupal\votingapi\VoteResultFunctionBase;

/**
 * The total number of 4 points votes.
 *
 * @VoteResultFunction(
 *   id = "points_4",
 *   label = @Translation("Points 4 votes"),
 *   description = @Translation("The number of votes with 4 points."),
 *   num_value = 4
 * )
 */
class Points4 extends VoteResultFunctionBase {

  /**
   * {@inheritdoc}
   */
  public function calculateResult($votes) {
    $sum = 0;
    foreach ($votes as $vote) {
      if ($vote->getValue() == 4) {
        $sum++;
      }
    }
    return $sum;
  }

}
