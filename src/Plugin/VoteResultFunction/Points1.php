<?php

namespace Drupal\vote\Plugin\VoteResultFunction;

use Drupal\votingapi\VoteResultFunctionBase;

/**
 * The total number of 1 point votes.
 *
 * @VoteResultFunction(
 *   id = "points_1",
 *   label = @Translation("Point 1 votes"),
 *   description = @Translation("The number of votes with 1 point."),
 *   num_value = 1
 * )
 */
class Points1 extends VoteResultFunctionBase {

  /**
   * {@inheritdoc}
   */
  public function calculateResult($votes) {
    $sum = 0;
    foreach ($votes as $vote) {
      if ($vote->getValue() == 1) {
        $sum++;
      }
    }
    return $sum;
  }

}
