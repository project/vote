<?php

namespace Drupal\vote\Plugin\VoteResultFunction;

use Drupal\votingapi\VoteResultFunctionBase;

/**
 * The total number of 0 points votes.
 *
 * @VoteResultFunction(
 *   id = "points_0",
 *   label = @Translation("Point 0 votes"),
 *   description = @Translation("The number of votes with 0 point."),
 *   num_value = 0
 * )
 */
class Points0 extends VoteResultFunctionBase {

  /**
   * {@inheritdoc}
   */
  public function calculateResult($votes) {
    $sum = 0;
    foreach ($votes as $vote) {
      if ($vote->getValue() == 0) {
        $sum++;
      }
    }
    return $sum;
  }

}
