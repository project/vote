<?php

namespace Drupal\vote\Plugin\VoteResultFunction;

use Drupal\votingapi\VoteResultFunctionBase;

/**
 * The total number of 5 points votes.
 *
 * @VoteResultFunction(
 *   id = "points_5",
 *   label = @Translation("Points 5 votes"),
 *   description = @Translation("The number of votes with 5 points."),
 *   num_value = 5
 * )
 */
class Points5 extends VoteResultFunctionBase {

  /**
   * {@inheritdoc}
   */
  public function calculateResult($votes) {
    $sum = 0;
    foreach ($votes as $vote) {
      if ($vote->getValue() == 5) {
        $sum++;
      }
    }
    return $sum;
  }

}
