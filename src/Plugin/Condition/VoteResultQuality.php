<?php

namespace Drupal\vote\Plugin\Condition;

use Drupal\rules\Core\RulesConditionBase;
use Drupal\votingapi\VoteInterface;
use Drupal\vote\Entity\VoteType;

/**
 * Provides a 'vote quality result' condition.
 *
 * In vote_vote_type entities you can define if a voting is good/neutral/bad.
 * This condition gives the possibility to react on the result quality.
 *
 * @Condition(
 *   id = "vote_result_quality",
 *   label = @Translation("Voting result quality"),
 *   category = @Translation("Vote"),
 *   context_definitions = {
 *     "vote" = @ContextDefinition("any",
 *       label = @Translation("Vote entity"),
 *       description = @Translation("The vote entity, the user has created or updated."),
 *       assignment_restriction = "selector"
 *     ),
 *     "type" = @ContextDefinition("string",
 *       label = @Translation("Vote Result Quality"),
 *       description = @Translation("Voting quality (good, neutral, bad) to assist rewards or required improvements."),
 *       options_provider = "\Drupal\vote\TypedData\Options\VoteResultQuality",
 *       assignment_restriction = "input"
 *     ),
 *   }
 * )
 */
class VoteResultQuality extends RulesConditionBase {

  /**
   * Evaluate the data comparison.
   *
   * @param \Drupal\votingapi\VoteInterface $vote
   *   Supplied vote entity to evaluate by plugin.
   *
   * @param string $type
   *   Voting quality.
   *   @see \Drupal\vote\TypedData\Options\VoteResultQuality.
   *
   * @return bool
   *   The evaluation of the condition.
   */
  protected function doEvaluate(VoteInterface $vote, string $type): bool {
    try {
      $entity_type = VoteType::load($vote->bundle());
    }
    catch (\Exception $e) {
      return FALSE;
    }
    $quality = $entity_type->getQuality($vote->getValue());
    return ($quality == $type);
  }

}
