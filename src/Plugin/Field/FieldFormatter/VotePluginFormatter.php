<?php

namespace Drupal\vote\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\Attribute;
use Drupal\vote\VotingApiService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'vote_plugin_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "vote_plugin_formatter",
 *   label = @Translation("Vote plugin formatter"),
 *   field_types = {
 *     "string_long"
 *   }
 * )
 */
class VotePluginFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * Sets the date formatter.
   *
   * @var \Drupal\vote\VotingApiService
   *   The date formatter service.
   */
  protected $votingApiService;

  /**
   * Instance creator extended with vote.votingapi service.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setVotingApiService($container->get('vote.votingapi'));
    return $instance;
  }

  /**
   * Sets voting api service.
   *
   * @param \Drupal\vote\VotingApiService $voting_api_service
   *   The voting api service.
   */
  protected function setVotingApiService(VotingApiService $voting_api_service) {
    $this->votingApiService = $voting_api_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();

    // Fall back to field settings by default.
    $settings['placement'] = 'auto';
    $settings['interactive'] = '1';
    $settings['voting_stats'] = '1';
    $settings['summary'] = 'detailed';

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['interactive'] = [
      '#type' => 'checkbox',
      '#title' => new TranslatableMarkup('Allow interactive display'),
      '#default_value' => $this->getSetting('interactive'),
      '#description' => new TranslatableMarkup('If checked, users can vote and see stats, if NOT just displays condensed result.'),
    ];

    $form['placement'] = [
      '#type' => 'select',
      '#title' => new TranslatableMarkup('Modal placement relative to opener (condensed widget).'),
      '#options' => [
        'auto' => new TranslatableMarkup('auto'),
        'auto-start' => new TranslatableMarkup('auto-start'),
        'auto-end' => new TranslatableMarkup('auto-end'),
        'top' => new TranslatableMarkup('top'),
        'top-start' => new TranslatableMarkup('top-start'),
        'top-end' => new TranslatableMarkup('top-end'),
        'bottom' => new TranslatableMarkup('bottom'),
        'bottom-start' => new TranslatableMarkup('bottom-start'),
        'bottom-end' => new TranslatableMarkup('bottom-end'),
        'right' => new TranslatableMarkup('right'),
        'right-start' => new TranslatableMarkup('right-start'),
        'right-end' => new TranslatableMarkup('right-end'),
        'left' => new TranslatableMarkup('left'),
        'left-start' => new TranslatableMarkup('left-start'),
        'left-end' => new TranslatableMarkup('left-end'),
      ],
      '#default_value' => $this->getSetting('placement'),
      '#description' => new TranslatableMarkup('In what direction the voting dialog should open by default? If there is not enough space in the viewport to show the complete modal, modal opens in other direction with sufficient space.'),
    ];

    $form['voting_stats'] = [
      '#type' => 'checkbox',
      '#title' => new TranslatableMarkup('Show voting stats in widget'),
      '#default_value' => $this->getSetting('voting_stats'),
      '#description' => new TranslatableMarkup('If checked, users can see voting stats after given their vote (further depends on permissions).'),
    ];

    $form['summary'] = [
      '#type' => 'select',
      '#title' => new TranslatableMarkup('Summary Display'),
      '#options' => [
        'detailed' => new TranslatableMarkup('Detailed'),
        'percent' => new TranslatableMarkup('Sum (percent)'),
        '5_star' => new TranslatableMarkup('Sum (5-Star)'),
      ],
      '#default_value' => $this->getSetting('summary'),
      '#description' => new TranslatableMarkup('Kind of display the condensed widget (the part what is always visible)'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    if ($this->getSetting('interactive')) {
      $summary[] = new TranslatableMarkup('Modal placement: %placement.', [
        '%placement' => $this->getSetting('placement'),
      ]);

      $summary[] = new TranslatableMarkup('Stats: %display.', [
        '%display' => $this->getSetting('voting_stats') ? 'shown' : 'hidden',
      ]);

      $summary[] = new TranslatableMarkup('Summary: %display.', [
        '%display' => $this->getSetting('summary'),
      ]);
    }
    else {
      $summary[] = new TranslatableMarkup('No interaction, just condensed result.');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function view(FieldItemListInterface $items, $langcode = NULL) {
    // Default the language to the current content language.
    if (empty($langcode)) {
      $langcode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    }
    $elements = $this->viewElements($items, $langcode);

    $entity = $items->getEntity();
    $entity_type_id = $entity->getEntityTypeId();
    $entity_id = $entity->id();

    $slug = "$entity_type_id:$entity_id";

    // If there are actual renderable children, use #theme => field, otherwise,
    // let access cacheability metadata pass through for correct bubbling.
    if (Element::children($elements)) {
      $field_name = $this->fieldDefinition->getName();
      $info = [
        '#theme' => 'vote_plugin',
        '#attached' => [
          'library' => ['vote/vote_library'],
          'drupalSettings' => [
            'votejsr' => [
              'votings' => [
                $slug => $this->votingApiService->getResults($entity_type_id, $entity_id),
              ],
            ],
          ],
        ],
        '#attributes' => [
          'class' => ['votejsr'],
          'data-entity-id' => $slug,
          'data-interactive' => $this->getSetting('interactive'),
          'data-placement' => $this->getSetting('placement'),
          'data-summary' => $this->getSetting('summary'),
          'data-votejsr' => "vote",
          'data-voting-stats' => $this->getSetting('voting_stats'),
        ],
        '#bundle' => $entity->bundle(),
        '#entity_type' => $entity_type_id,
        '#field_attributes' => new Attribute([]),
        '#field_name' => $field_name,
        '#field_translatable' => $this->fieldDefinition->isTranslatable(),
        '#field_type' => $this->fieldDefinition->getType(),
        '#formatter' => $this->getPluginId(),
        '#items' => $elements,
        '#is_multiple' => $this->fieldDefinition->getFieldStorageDefinition()->isMultiple(),
        '#label' => $this->fieldDefinition->getLabel(),
        '#label_display' => $this->label,
        '#label_hidden' => ($this->label == 'hidden'),
        '#language' => $items->getLangcode(),
        '#object' => $entity,
        '#third_party_settings' => $this->getThirdPartySettings(),
        '#view_mode' => $this->viewMode,
      ];

      $elements = array_merge($info, $elements);
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = $this->viewValue($item);
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return array
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    $value = $item->getValue();
    return [
      '#theme' => 'vote_condensed',
      '#icon' => $value['icon'],
      '#condensed' => $value['condensed'],
      '#attributes' => new Attribute([
        'class' => [
          'vjsr--condensed--widget--' . $value['type'],
        ],
        'title' => strip_tags($value['value']),
      ]),
      '#attached' => [
        'drupalSettings' => [
          'votejsr' => [
            'votingTypes' => [
              /** @var \Drupal\vote\Entity\VoteType $value[vote_type] */
              "{$value['vote_type']['id']}" => $value['vote_type'],
            ],
          ],
        ],
      ],
    ];
  }

}
