<?php

namespace Drupal\vote\Plugin\ECA\Condition;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Plugin\ECA\Condition\ConditionBase;
use Drupal\vote\TypedData\Options\VoteResultQuality as Quality;
use Drupal\votingapi\VoteInterface;
use Drupal\vote\Entity\VoteType;

/**
 * Provides a 'vote quality result' condition.
 *
 * In vote_vote_type entities you can define if a voting is good/neutral/bad.
 * This condition gives the possibility to react on the result quality.
 *
 * @EcaCondition(
 *   id = "vote_result_quality",
 *   label = @Translation("Voting result quality."),
 *   description = @Translation("In vote_vote_type entities you can define if a voting is good/neutral/bad. This condition gives the possibility to react on the result quality."),
 *   context_definitions = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Entity")
 *     )
 *   }
 * )
 */
class VoteResultQuality extends ConditionBase {

  /**
   * Evaluate the data comparison.
   *
   * @return bool
   *   The evaluation of the condition.
   */
  public function evaluate(): bool {
    try {
      $entity = $this->getValueFromContext('entity');
      if (!($entity instanceof VoteInterface)) {
        throw new \Exception('Condition could not evaluate, entity not of type VoteInterface.');
      }
      $entity_type = VoteType::load($entity->bundle());
    }
    catch (\Exception $e) {
      \Drupal::logger('vote')->error($e->getMessage());
      return FALSE;
    }
    $expected_quality = $this->configuration['quality'];
    $quality = $entity_type->getQuality($entity->getValue());
    return $this->negationCheck($quality == $expected_quality);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
        'quality' => Quality::QUALITY_NEUTRAL,
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['quality'] = [
      '#type' => 'select',
      '#title' => $this->t('Vote Quality'),
      '#description' => $this->t('Enter the expected vote quality.'),
      '#default_value' => $this->configuration['quality'],
      '#options' => Quality::getOptions(),
      '#required' => TRUE,
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['quality'] = $form_state->getValue('quality');
    parent::submitConfigurationForm($form, $form_state);
  }

}
