<?php

namespace Drupal\vote\Plugin\VoteBaseType;

use Drupal\vote\VoteBaseTypeBase;

/**
 * Provides a Plugin of type five_star.
 *
 * @VoteBaseType(
 *   id = "five_star",
 *   label = @Translation("Five Star"),
 *   description = @Translation("Base class for Five Star widgets."),
 *   value_definition = {
 *     "points_5" = 5,
 *     "points_4" = 4,
 *     "points_3" = 3,
 *     "points_2" = 2,
 *     "points_1" = 1
 *   },
 *   labels = {
 *     "points_1" = "1",
 *     "points_2" = "2",
 *     "points_3" = "3",
 *     "points_4" = "4",
 *     "points_5" = "5"
 *   },
 *   condensed_format = "%result/5",
 *   result_format = "Stars: %result/5",
 * )
 */
class VoteBaseTypeFiveStar extends VoteBaseTypeBase {

  /**
   * {@inheritdoc}
   */
  public function getResultString(array $raw_result, $vote_type, $default_value = "0") {
    if (!array_key_exists('vote_average', $raw_result)) {
      return "0";
    }
    $res_num = $raw_result['vote_average'];
    return (string) number_format((float) $res_num, 1);
  }

}
