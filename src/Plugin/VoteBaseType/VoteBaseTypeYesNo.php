<?php

namespace Drupal\vote\Plugin\VoteBaseType;

use Drupal\vote\VoteBaseTypeBase;

/**
 * Provides a Plugin of type yes_no.
 *
 * @VoteBaseType(
 *   id = "yes_no",
 *   label = @Translation("Yes-No"),
 *   description = @Translation("Base class for Yes-No widgets."),
 *   value_definition = {
 *     "points_1" = 1,
 *     "points_0" = 0,
 *     "points_n1" = -1,
 *   },
 *   labels = {
 *     "points_1" = "Yes",
 *     "points_0" = "Abstain",
 *     "points_n1" = "No",
 *   },
 *   condensed_format = "%result",
 *   result_format = "Result: %result",
 * )
 */
class VoteBaseTypeYesNo extends VoteBaseTypeBase {

  /**
   * {@inheritdoc}
   */
  public function getResultString(array $raw_result, $vote_type, $default_value = "0") {
    $labels = $vote_type->get('labels') ?? [];
    foreach (['points_1', 'points_n1', 'vote_count'] as $key) {
      if (!array_key_exists($key, $raw_result)) {
        return $this->t('Draw');
      }
    }
    if ($raw_result['points_1'] == $raw_result['points_n1']) {
      // Early return of Draw.
      return $this->t('Draw');
    }
    elseif (!!$vote_type->get('allow_abstention')) {
      $min = $raw_result['vote_count'] / 2;
      if ($raw_result['points_n1'] >= $min) {
        $best_key = 'points_n1';
      }
      elseif ($raw_result['points_1'] >= $min) {
        $best_key = 'points_1';
      }
      else {
        return $this->t('No majority');
      }
    }
    else {
      $best_key = ($raw_result['points_1'] > $raw_result['points_n1']) ? 'points_1' : 'points_n1';
    }

    return (array_key_exists($best_key, $labels)) ? $labels[$best_key] : ucfirst($best_key);
  }

}
