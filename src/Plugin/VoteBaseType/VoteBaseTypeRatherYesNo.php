<?php

namespace Drupal\vote\Plugin\VoteBaseType;

use Drupal\vote\VoteBaseTypeBase;

/**
 * Provides a Plugin of type rather_yes_no.
 *
 * @VoteBaseType(
 *   id = "rather_yes_no",
 *   label = @Translation("Rather Yes-No"),
 *   description = @Translation("Base class for Rather Yes-No widgets."),
 *   value_definition = {
 *     "points_2" = 2,
 *     "points_1" = 1,
 *     "points_0" = 0,
 *     "points_n1" = -1,
 *     "points_n2" = -2,
 *   },
 *   labels = {
 *     "points_2" = "Yes",
 *     "points_1" = "Rather yes",
 *     "points_0" = "Abstain",
 *     "points_n1" = "Rather no",
 *     "points_n2" = "No",
 *   },
 *   condensed_format = "%result",
 *   result_format = "Result: %result",
 * )
 */
class VoteBaseTypeRatherYesNo extends VoteBaseTypeBase {

  /**
   * {@inheritdoc}
   */
  public function getResultString(array $raw_result, $vote_type, $default_value = "0") {
    $plugin_def = $this->getPluginDefinition();
    $value_def = $plugin_def['value_definition'] ?? [];
    $labels = $vote_type->get('labels') ?? [];
    // Set a default value if vote_average is not present.
    $result = $raw_result['vote_average'] ?? 0;

    if ($result === 0) {
      return $this->t('Draw');
    }

    // Check if $result is a valid number before rounding.
    if (!is_numeric($result)) {
      // Handle the case when $result is not a number.
      return $default_value;
    }

    // Round the $result to the nearest integer.
    $result = round($result);

    foreach ($value_def as $key => $value) {
      if ($result == $value) {
        return (array_key_exists($key, $labels)) ? $labels[$key] : ucfirst($key);
      }
    }
    return $default_value;
  }

}
