<?php

namespace Drupal\vote;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\vote\Entity\VoteTypeInterface;

/**
 * Manages VoteBaseType plugins.
 */
class VoteBaseTypeManager extends DefaultPluginManager {

  /**
   * Collection of used VoteBaseType instances.
   *
   * @var VoteBaseTypeInterface[]
   */
  protected $pluginInstances = [];

  /**
   * Constructs a new \Drupal\Core\Block\BlockManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct('Plugin/VoteBaseType', $namespaces, $module_handler, 'Drupal\vote\VoteBaseTypeInterface', 'Drupal\vote\Annotation\VoteBaseType');
    $this->alterInfo('vote_base_type_info');
    $this->setCacheBackend($cache_backend, 'vote_base_type_plugins');
  }

  /**
   * Get an instance of VoteBaseType.
   *
   * @param string $plugin_id
   *   The base type plugin id.
   *
   * @return VoteBaseTypeInterface|object|null
   *   Returns a plugin instance from base type.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getPluginInstance(string $plugin_id) {
    if (!isset($this->pluginInstances[$plugin_id])) {
      $this->pluginInstances[$plugin_id] = $this->createInstance($plugin_id);
    }
    return $this->pluginInstances[$plugin_id] ?: NULL;
  }

  /**
   * Get the result for a single voting challenge.
   *
   * @param \Drupal\vote\Entity\VoteTypeInterface $vote_type
   *   Plugin id for vote_base_type.
   * @param array $raw_result
   *   The raw result from voting API.
   *
   * @return array|null
   *   Returns a complete voting result.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getTypedResult(VoteTypeInterface $vote_type, array $raw_result) {
    $base_type = $vote_type->get('base_type');
    if ($base_plugin = $this->getPluginInstance($base_type)) {
      return [
        'result' => $base_plugin->getResultString($raw_result, $vote_type),
        'raw' => $base_plugin->getResultData($raw_result, 'vote_average', 1),
        'abs' => $base_plugin->getTotalResult($raw_result),
        'votes' => $base_plugin->getOptionsResult($raw_result),
        'votesSum' => $base_plugin->getResultData($raw_result, 'vote_count'),
        'ownVote' => '',
      ];
    }
    return NULL;
  }

  /**
   * Get the result function id from a voting value.
   *
   * @param string $base_type
   *   The base type id of used plugin.
   * @param int $value
   *   The value to identify.
   *
   * @return string
   *   The result function id for this value.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getResultKeyFromValue(string $base_type, int $value) {
    if ($base_plugin = $this->getPluginInstance($base_type)) {
      return $base_plugin->getResultKeyFromValue($value);
    }
    else {
      return '';
    }
  }

}
