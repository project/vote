<?php

namespace Drupal\vote;

use Drupal\vote\Entity\VoteTypeInterface;

/**
 * Provides an interface for a VoteResultFunction plugin.
 *
 * @see \Drupal\votingapi\Annotation\VoteResultFunction
 * @see \Drupal\votingapi\VoteManager
 * @see \Drupal\votingapi\VoteBaseTypeBase
 * @see plugin_api
 */
interface VoteBaseTypeInterface {

  /**
   * Get the default formatter .
   *
   * @return string
   *   The option to numeric value mapping.
   */
  public function getCondensedFormatter();

  /**
   * Retrieve the description for the voting result.
   *
   * @return string
   *   The translated description
   */
  public function getDescription();

  /**
   * Retrieve the label for the voting result.
   *
   * @return string
   *   The translated label
   */
  public function getLabel();

  /**
   * Get the options result: num of votes for each option.
   *
   * @param array $votes
   *   An array of Vote entities.
   *
   * @return array
   *   A result based on the supplied votes.
   */
  public function getOptionsResult(array $votes);

  /**
   * Returns mapping of vote options and numeric value.
   *
   * @return string
   *   The option to numeric value mapping.
   */
  public function getResultFormatter();

  /**
   * Get a result value from raw result by result function id.
   *
   * @param array $raw_result
   *   Result collection for target entity.
   * @param string $result_id
   *   Target entity id.
   * @param int $precision
   *   Rounding precision.
   * @param mixed $default_value
   *   Value to use if no result.
   *
   * @return int|float|null
   *   The requested result function item.
   */
  public function getResultData(array $raw_result, string $result_id, int $precision = 0, $default_value = 0);

  /**
   * Get a Voting API result function id from unique numeric value.
   *
   * @param int $result
   *   Target entity id.
   *
   * @return string
   *   The Voting API result function id.
   */
  public function getResultKeyFromValue(int $result);

  /**
   * Get a result function item from raw result.
   *
   * @param array $raw_result
   *   Result collection for target entity.
   * @param \Drupal\vote\Entity\VoteTypeInterface $vote_type
   *   The vote type built on the plugin.
   * @param mixed $default_value
   *   Value to use if no result.
   *
   * @return string|null
   *   Result as human-readable string.
   */
  public function getResultString(array $raw_result, VoteTypeInterface $vote_type, $default_value = "0");

  /**
   * Get the total result used for sorting.
   *
   * @param array $raw_result
   *   Result collection for target entity.
   *
   * @return float
   *   A value >= 0 and <= 1.
   */
  public function getTotalResult(array $raw_result);

  /**
   * Returns mapping of vote options and numeric value.
   *
   * @return array
   *   The option to numeric value mapping.
   */
  public function getValuesDefinition();

}
