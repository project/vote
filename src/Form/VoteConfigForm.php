<?php

namespace Drupal\vote\Form;

use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The module config form.
 */
class VoteConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Config\ConfigManagerInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;

  /**
   * ContentEntityTypes potentially supported by vote module.
   *
   * @var \Drupal\Core\Entity\ContentEntityTypeInterface[]
   */
  protected $filteredContentEntityTypes;

  /**
   * List of available modules.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $extensionList;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->configFactory = $container->get('config.factory');
    $instance->configManager = $container->get('config.manager');
    $instance->bundleInfo = $container->get('entity_type.bundle.info');
    $instance->extensionList = $container->get('extension.list.module');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vote.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vote_config_form';
  }

  /**
   * Callback function for Ajax form update.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The updated form.
   */
  public function voteGetBundleForm(array $form, FormStateInterface &$form_state) {
    $trigger_element = $form_state->getTriggeringElement();
    $id = $trigger_element['#ajax']['entity_id'];
    return $form["vote_entity_map_{$id}"]["entity_type_{$id}_bundles"];
  }

  /**
   * Get entity types potentially supported by vote module.
   *
   * @return \Drupal\Core\Entity\ContentEntityTypeInterface[]
   *   Returns a filtered array of content entity types that may have voting.
   */
  protected function getFilteredContentEntityTypes() {
    if (!$this->filteredContentEntityTypes) {
      $entity_types = $this->entityTypeManager->getDefinitions();
      // Filter for unusable entity types.
      $unusable = ['access_token', 'content_moderation_state', 'crop', 'menu_link_content', 'path_alias', 'redirect',
        'update_helper_checklist_update', 'vote', 'vote_result',
      ];

      foreach ($entity_types as $id => $entity_type) {
        if (!($entity_type instanceof ContentEntityType) || in_array($id, $unusable)) {
          unset($entity_types[$id]);
        }
      }
      $this->filteredContentEntityTypes = $entity_types;
    }
    return $this->filteredContentEntityTypes;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vote.config');

    $form['vote_entity_map'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Entity to Voting types map'),
    ];

    foreach ($this->getFilteredContentEntityTypes() as $id => $entity_type) {

      $form["vote_entity_map_{$id}"] = [
        '#type' => 'details',
        '#title' => $entity_type->getLabel(),
        '#group' => 'vote_entity_map',
      ];

      $form["vote_entity_map_{$id}"]["entity_type_{$id}_enabled"] = [
        '#type' => 'checkbox',
        '#title' => $entity_type->getLabel(),
        '#description' => $this->t('Check to enable %ens for voting.', ['%ens' => $entity_type->getLabel()]),
        '#default_value' => is_array($config->get("entity_vote_map.{$id}")),
        '#ajax' => [
          'callback' => '::voteGetBundleForm',
          'wrapper' => "entity_type_{$id}_bundles",
          'event' => 'change',
          'method' => 'replace',
          'entity_id' => $id,
        ],
      ];

      if ($form_state->getValue("entity_type_{$id}_enabled", is_array($config->get("entity_vote_map.{$id}")))) {
        $form["vote_entity_map_{$id}"]["entity_type_{$id}_bundles"] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Add voting types for each bundle of %entity', ['%entity' => $entity_type->getLabel()]),
          '#prefix' => "<div id=\"entity_type_{$id}_bundles\">",
          '#suffix' => "</div>",
        ];
        foreach ($this->bundleInfo->getBundleInfo($id) as $bundleId => $bundle) {
          $bundle_voting_types = $config->get("entity_vote_map.{$id}.{$bundleId}") ?: [];
          $default_value = $this->entityTypeManager->getStorage('vote_vote_type')
            ->loadMultiple($bundle_voting_types);

          $form["vote_entity_map_{$id}"]["entity_type_{$id}_bundles"]["entity_type_{$id}_{$bundleId}_bundle"] = [
            '#type' => "entity_autocomplete",
            '#target_type' => 'vote_vote_type',
            '#title' => $this->t('Voting types for %bundle', [
              '%bundle' => $bundle['label'],
            ]),
            '#description' => $this->t('Add (multiple) voting types by typing first chars of the voting type title, and select the correct when it appears.'),
            '#multiple' => TRUE,
            '#tags' => TRUE,
            '#default_value' => $default_value,
          ];
        }

      }
      else {
        $form["vote_entity_map_{$id}"]["entity_type_{$id}_bundles"] = [
          '#markup' => $this->t("Enable entity type %type to add voting types.", [
            '%type' => $entity_type->getLabel(),
          ]),
          '#prefix' => "<div id=\"entity_type_{$id}_bundles\">",
          '#suffix' => "</div>",
        ];
      }

      $entity_types_options[$id] = $entity_type->getLabel();

    }

    $form["vote_api"] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API'),
    ];

    $form["vote_api"]['method'] = [
      '#type' => 'select',
      '#title' => $this->t('Method'),
      '#options' => [
        'POST' => 'POST',
        'GET' => 'GET',
        'PATCH' => 'PATCH',
        'PUT' => 'PUT',
        'DELETE' => 'DELETE',
      ],
      '#description' => $this->t('Configure the method for API calls from frontend widgets. (default: POST)'),
      '#default_value' => $config->get('api.request_options.method'),
    ];

    $form["vote_api"]['headers'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Request Headers'),
      '#description' => $this->t('Enter request headers in key-pipe-value-line notation. (default: /vote/vote-results)'),
      '#default_value' => _vote_arr2str_key_val($config->get('api.request_options.headers') ?? []),
    ];

    $form["vote_api"]['results'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Result path'),
      '#description' => $this->t('Path from where the results were fetched. (default: /vote/vote-results)'),
      '#default_value' => $config->get('api.results'),
    ];

    $form["vote_api"]['vote'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Vote requests path'),
      '#description' => $this->t('Path from where the results were fetched. (default: /vote/vote/{entity_type_id}/{entity_id})'),
      '#default_value' => $config->get('api.vote'),
    ];

    $form["icons"] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Icons'),
    ];

    $mod_path = $this->extensionList->getPath('vote') ?? '';
    $mod_path = '/' . $mod_path;
    $form["icons"]['default_sprite'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Icon Sprite'),
      '#description' => $this->t('Path to icon SVG-sprite. (default: %path)', [
        '%path' => $mod_path . '/assets/svg/icons.svg',
      ]),
      '#default_value' => $config->get('icons.default_sprite'),
    ];

    $form["button_types"] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Buttons'),
    ];

    $form["button_types"]['btn_default'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Button CSS-Class'),
      '#description' => $this->t('Classes added to buttons of type Default. E.g. Action Buttons in the modal footer.'),
      '#default_value' => $config->get('button_types.default'),
    ];

    $form["button_types"]['btn_primary'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Primary Button CSS-Class'),
      '#description' => $this->t('Classes added to buttons of type Primary.'),
      '#default_value' => $config->get('button_types.primary'),
    ];

    $form["button_types"]['btn_secondary'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secondary Button CSS-Class'),
      '#description' => $this->t('Classes added to buttons of type Secondary. E.g. used in YesNo challenges.'),
      '#default_value' => $config->get('button_types.secondary'),
    ];

    $form["button_types"]['btn_tertiary'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tertiary Button CSS-Class'),
      '#description' => $this->t('Classes added to buttons of type Tertiary. E.g. used in multiple challenges.'),
      '#default_value' => $config->get('button_types.tertiary'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $voteconfig = $this->config('vote.config');
    $entity_vote_map = [];

    foreach ($this->getFilteredContentEntityTypes() as $id => $entity_type) {
      $entity_enabled = $form_state->getValue("entity_type_{$id}_enabled");

      if (!!$entity_enabled) {
        $entity_vote_map[$id] = [];
        foreach ($this->bundleInfo->getBundleInfo($id) as $bundleId => $bLabel) {
          $map = [];
          $map_raw = $form_state->getValue("entity_type_{$id}_{$bundleId}_bundle") ?: [];
          foreach ($map_raw as $value) {
            $map[] = $value['target_id'];
          }
          if (count($map)) {
            $entity_vote_map[$id][$bundleId] = $map;
          }
        }
      }
    }

    $voteconfig->set('entity_vote_map', $entity_vote_map);
    $voteconfig->set('api.request_options.method', $form_state->getValue("method"));
    $voteconfig->set('api.request_options.headers', _vote_str2arr_key_val($form_state->getValue("headers")));
    $voteconfig->set('api.results', $form_state->getValue("results"));
    $voteconfig->set('api.vote', $form_state->getValue("vote"));
    $voteconfig->set('icons.default_sprite', $form_state->getValue("default_sprite"));
    $voteconfig->set('button_types.default', $form_state->getValue("btn_default"));
    $voteconfig->set('button_types.primary', $form_state->getValue("btn_primary"));
    $voteconfig->set('button_types.secondary', $form_state->getValue("btn_secondary"));
    $voteconfig->set('button_types.tertiary', $form_state->getValue("btn_tertiary"));
    $voteconfig->save();
  }

}
