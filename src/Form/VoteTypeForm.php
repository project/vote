<?php

namespace Drupal\vote\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\vote\VoteBaseTypeManager;
use Drupal\vote\VotingApiService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the add/edit form for vote_vote_type config entities.
 */
class VoteTypeForm extends EntityForm implements ContainerInjectionInterface {

  /**
   * The vote base type manager.
   *
   * @var \Drupal\vote\VoteBaseTypeManager
   */
  protected $voteBaseTypeManager;

  /**
   * Drupal\vote\VotingApiService definition.
   *
   * @var \Drupal\vote\VotingApiService
   */
  protected $votingApiService;

  /**
   * The baste type definitions.
   *
   * @var array
   */
  protected $baseTypesDefinitions;

  /**
   * Constructor of the form extends the instance with further services.
   */
  public function __construct(VoteBaseTypeManager $vote_base_type_manager, VotingApiService $voting_api_service) {
    $this->voteBaseTypeManager = $vote_base_type_manager;
    $this->votingApiService = $voting_api_service;
  }

  /**
   * The class creator extends the instance with further services.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.vote_base_type'),
      $container->get('vote.votingapi')
    );
  }

  /**
   * Callback for Ajax form updates.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   The updated part of the form render array.
   */
  public function specificBundleForm(array $form, FormStateInterface &$form_state) {
    return $form["base_type_specs"];
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $vote_vote_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $vote_vote_type->label(),
      '#description' => $this->t("Label for the Vote type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $vote_vote_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\vote\Entity\VoteType::load',
      ],
      '#disabled' => !$vote_vote_type->isNew(),
    ];

    // Check if this voting type already has votes.
    $has_vote_type_votes = $vote_vote_type->id() && $this->votingApiService->hasVoteTypeVotes($vote_vote_type->id());

    // Get Vote-Types as option.
    $voteBaseTypeOptions = [];
    foreach ($this->getBaseTypesDefinitions() as $key => $value) {
      $voteBaseTypeOptions[$key] = $value['label'];
    }
    $form['base_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Voting widget'),
      '#default_value' => $vote_vote_type->get('base_type'),
      '#options' => $voteBaseTypeOptions,
      '#required' => TRUE,
      '#empty_option' => $this->t('Select widget'),
      '#empty_value' => '',
      '#disabled' => $has_vote_type_votes,
      '#description' => ($has_vote_type_votes)
      ? $this->t('Field disabled because votes of this type already exists. Delete votes manually to change this field.')
      : $this->t('Select a base type for this voting type.<br>IMPORTANT: If you changed voting widget, save before continue to edit the form.'),
      '#ajax' => [
        'callback' => '::specificBundleForm',
        'wrapper' => 'base_type_specs',
        'event' => 'change',
        'method' => 'replace',
      ],
    ];

    $form['question'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Question'),
      '#default_value' => $vote_vote_type->get('question') ?: '',
      '#size' => 60,
      '#maxlength' => 128,
      '#description' => $this->t("Enter the question to answer by the users vote."),
    ];

    $form['rating_factor'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Rating factor'),
      '#default_value' => $vote_vote_type->get('rating_factor') | 1,
      '#description' => $this->t("The rating factor defines the order weight of this voting type in combination
         with other voting types on same entity. The higher the number, the more important becomes this voting type.<br>
         (default: 1)"),
    ];

    // Get the base type for default settings.
    $base_type_id = $form_state->getValue('base_type', $vote_vote_type->get('base_type'));
    $base_type = ($base_type_id) ? $this->getBaseTypesDefinition($base_type_id) : NULL;

    $form["base_type_specs"] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Vote widget settings'),
      '#prefix' => "<div id=\"base_type_specs\">",
      '#suffix' => "</div>",
    ];

    if (is_array($base_type)) {
      $allow_abstention = !!$vote_vote_type->get('allow_abstention');
      if (isset($base_type['value_definition']['points_0'])) {
        $form["base_type_specs"]['allow_abstention'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Abstention allowed'),
          '#default_value' => $vote_vote_type->get('allow_abstention'),
          '#description' => $this->t('Voting type allows to select the 0 value (not decided). Has no effect, if widget type not includes the 0 value e.g. Five Star.'),
          '#ajax' => [
            'callback' => '::specificBundleForm',
            'wrapper' => 'base_type_specs',
            'event' => 'change',
            'method' => 'replace',
          ],
        ];
      }

      // Ajax call changes the label set.
      $labels = "";
      $saved_labels = $vote_vote_type->get('labels') ?: [];
      $default_labels = $base_type['labels'];
      foreach ($default_labels as $key => $default_value) {
        if ($key == 'points_0' && !$allow_abstention) {
          continue;
        }
        $value = (isset($saved_labels[$key])) ? $saved_labels[$key] : $default_value;
        $labels = $labels . "{$key}|{$value}\n";
      }

      $form["base_type_specs"]['labels'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Options / labels'),
        '#default_value' => $labels,
        '#description' => $this->t("Enter a key|Value list (one item per line) with first part the machine_name
          (only lowercase chars, numbers and underscore _) divided by pipe | from the label (all text characters).
          <br> E.g. 'yes|I say Yes'."),
      ];

      // CONDENSED FORMAT.
      $condense_default = ($base_type) ? $base_type['condensed_format'] : '';
      $form_state->set('condensed_format', $base_type['condensed_format']);

      $form['base_type_specs']['condensed_format'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Condensed result formatter'),
        '#default_value' => $vote_vote_type->get('condensed_format') ?: $condense_default,
        '#size' => 60,
        '#maxlength' => 128,
        '#description' => $this->t("Translatable string with placeholders (%placeholders) for the numeric results.
          Used for the most reduced result display. (default: '%result').",
          ['%placeholders' => "%result, %raw, %own, %sum"]),
      ];

      // RESULT FORMAT.
      $result_format = ($base_type) ? $base_type['result_format'] : '';
      $form_state->set('result_format', $base_type['result_format']);

      $form['base_type_specs']['result_format'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Result formatter'),
        '#default_value' => $vote_vote_type->get('result_format') ?: $result_format,
        '#size' => 60,
        '#maxlength' => 128,
        '#description' => $this->t("Translatable string with placeholders (%placeholders) for the numeric results.
          Used for result display in voting stats (default: 'Result: %result').",
          ['%placeholders' => "%result, %raw, %own, %sum"]),
      ];
    }

    $form['display_options'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Display options'),
      '#collapsable' => FALSE,
    ];

    $form['display_options']['icon_family'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Icon font family'),
      '#default_value' => $vote_vote_type->get('icon_family'),
      '#size' => 60,
      '#maxlength' => 128,
      '#description' => $this->t("You can use icons from your custom icon font for this voting type.
         Enter the font-family name here."),
    ];

    $form['display_options']['icon'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Icon id'),
      '#default_value' => $vote_vote_type->get('icon'),
      '#size' => 60,
      '#maxlength' => 128,
      '#description' => $this->t("Enter an icon id string."),
    ];

    $form['display_options']['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description / Help text'),
      '#default_value' => $vote_vote_type->get('description'),
      '#rows' => 3,
      '#description' => $this->t("Enter a detailed help text what this voting type is asking for."),
    ];

    $form['display_options']['color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Highlight color'),
      '#default_value' => $vote_vote_type->get('color'),
      '#size' => 60,
      '#maxlength' => 64,
    ];

    $form["quality_props"] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Quality settings'),
      '#prefix' => "<div id=\"quality_props\">",
      '#suffix' => "</div>",
      '#description' => $this->t("Define quality bench marks. Define a value below which a result must be described as bad, and another value above which a value is described as good. A result between these values is called neutral. If a voting type does not allow classification according to good/bad, select lowest for Bad and highest for good. So the result is always neutral."),
    ];

    $form['quality_props']['quality_neutral_min'] = [
      '#type' => 'number',
      '#title' => $this->t('Bad (is smaller)'),
      '#default_value' => $vote_vote_type->get('quality_neutral_min') ?? -10,
      '#min' => -10,
      '#max' => 10,
      '#step' => 1
    ];

    $form['quality_props']['quality_neutral_max'] = [
      '#type' => 'number',
      '#title' => $this->t('Good (is greater)'),
      '#default_value' => $vote_vote_type->get('quality_neutral_max') ?? 10,
      '#min' => -10,
      '#max' => 10,
      '#step' => 1
    ];

    return $form;
  }

  /**
   * Lazy loader for base type defintion from plugin.
   *
   * @return array|mixed[]|null
   *   The base type definitions from plugin annotations.
   */
  protected function getBaseTypesDefinitions() {
    if (!$this->baseTypesDefinitions) {
      $this->baseTypesDefinitions = $this->voteBaseTypeManager->getDefinitions();
    }
    return $this->baseTypesDefinitions;
  }

  /**
   * Get base type definition from plugin annotations.
   *
   * @param string $base_type_id
   *   The vote base type id of vote_vote_type.
   *
   * @return mixed|null
   *   Returns the base type definition or null if not exists.
   */
  protected function getBaseTypesDefinition(string $base_type_id) {
    $base_types = $this->getBaseTypesDefinitions();
    return $base_types[$base_type_id] ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $vote_vote_type = $this->entity;

    // Get value definition from base_type.
    $base_type = $this->getBaseTypesDefinition($vote_vote_type->get('base_type'));
    $vote_vote_type->set('value_definition', $base_type['value_definition']);

    // Shape labels to array.
    $labels = [];
    foreach (explode("\n", $vote_vote_type->get('labels')) as $line) {
      $frags = explode("|", $line);
      if (count($frags) >= 2) {
        $labels[$frags[0]] = trim($frags[1]);
      }
    };
    $vote_vote_type->set('labels', $labels);

    $status = $vote_vote_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Vote type.', [
          '%label' => $vote_vote_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Vote type.', [
          '%label' => $vote_vote_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($vote_vote_type->toUrl('collection'));
  }

}
