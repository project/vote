<?php

namespace Drupal\vote;

use Drupal\Core\Plugin\PluginBase;
use Drupal\vote\Entity\VoteTypeInterface;

/**
 * Base class for plugins which provide a function to compute the vote result.
 */
abstract class VoteBaseTypeBase extends PluginBase implements VoteBaseTypeInterface {

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->t($this->pluginDefinition['label']);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t($this->pluginDefinition['description']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCondensedFormatter() {
    return $this->t($this->pluginDefinition['condensed_format']);
  }

  /**
   * {@inheritdoc}
   */
  public function getOptionsResult(array $raw_result) {
    $option_result = [];
    foreach ($this->pluginDefinition['value_definition'] as $key => $value) {
      $option_result[$key] = $this->getResultData($raw_result, $key);
    }
    return $option_result;
  }

  /**
   * {@inheritdoc}
   */
  public function getResultFormatter() {
    return $this->t($this->pluginDefinition['resultFormatter']);
  }

  /**
   * {@inheritdoc}
   */
  public function getResultData(array $raw_result, string $result_id, int $precision = 0, $default_value = 0) {
    if (isset($raw_result[$result_id])) {
      if ($precision) {
        $value = (float) $raw_result[$result_id];
        return round($value, $precision);
      }
      else {
        return (int) $raw_result[$result_id];
      }
    }
    return $default_value;
  }

  /**
   * {@inheritdoc}
   */
  public function getResultKeyFromValue(int $result) {
    foreach ($this->pluginDefinition['value_definition'] as $key => $value) {
      if ($result == $value) {
        return $key;
      }
    }
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getResultString(array $raw_result, VoteTypeInterface $vote_type, $default_value = "0") {
    $num_res = $raw_result['vote_average'];
    $best_key = NULL;
    $best_diff = NULL;
    foreach ($this->pluginDefinition['value_definition'] as $key => $value) {
      $curr_diff = abs($num_res - $value);
      if (!$best_key) {
        $best_key = $key;
        $best_diff = $curr_diff;
      }
      elseif ($curr_diff < $best_diff) {
        $best_key = $key;
        $best_diff = $curr_diff;
      }
    }
    if ($best_key) {
      $labels = $this->pluginDefinition['labels'];
      return $labels[$best_key] ?? ucfirst($best_key);
    }
    else {
      return $default_value;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalResult(array $raw_result, int $precision = 4) {
    $def = $this->getValuesDefinition();
    if (count($def) && isset($raw_result['vote_count'])) {
      $count = 0;
      $sum = 0;
      asort($def, SORT_NUMERIC);
      foreach ($def as $result_id => $value) {
        $min = (!isset($min) || $value < $min) ? (int) $value : $min;
        $max = (!isset($max) || $value > $max) ? (int) $value : $max;
        $count = (isset($raw_result[$result_id]))
          ? $count + $raw_result[$result_id] : $count;
        $sum = (isset($raw_result[$result_id]))
          ? $sum + $raw_result[$result_id] * $value : $sum;
      }
      $min_res = (isset($min)) ? $min * $raw_result['vote_count'] : 1;
      $max_res = (isset($max)) ? $max * $raw_result['vote_count'] : 1;
      $max_res = $max_res - $min_res;
      $sum = $sum - $min_res;
      return round($sum / $max_res, $precision);
    }
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getValuesDefinition() {
    return $this->pluginDefinition['value_definition'];
  }

}
