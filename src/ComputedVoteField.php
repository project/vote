<?php

namespace Drupal\vote;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TypedDataInterface;

/**
 * Voting widgets are attached to entity views by this computed field.
 */
class ComputedVoteField extends FieldItemList implements FieldItemListInterface {

  use ComputedItemListTrait;

  /**
   * Config of entity voting type mapping.
   *
   * @var array|null
   */
  protected $entityVoteMap;

  /**
   * Vote type config entities.
   *
   * @var \Drupal\vote\Entity\VoteTypeInterface[]
   */
  protected $voteTypes;

  /**
   * The voting api service.
   *
   * @var VotingApiService
   */
  protected $votingApiService;

  /**
   * Constructor that implements vote specific services and configs.
   */
  public function __construct(DataDefinitionInterface $definition, $name = NULL, TypedDataInterface $parent = NULL) {
    parent::__construct($definition, $name, $parent);
    // @todo load services from ContainerInterface
    $this->entityVoteMap = \Drupal::config('vote.config')->get('entity_vote_map') ?: ['entity_vote_map' => []];
    $this->voteTypes = \Drupal::entityTypeManager()->getStorage('vote_vote_type')->loadMultiple();
    $this->votingApiService = \Drupal::service('vote.votingapi');
  }

  /**
   * Compute the vote field value list.
   */
  protected function computeValue() {
    $entity = $this->getEntity();
    $entity_type_id = $entity->getEntityTypeId();
    $entity_id = $entity->id();
    $bundle = $entity->bundle();

    $values = [];

    if (isset($this->entityVoteMap[$entity_type_id][$bundle])) {
      // A new created entity has no ID - and no results.
      $results = ($entity_id)
        ? $this->votingApiService->getResults($entity_type_id, $entity_id, TRUE)
        : [];

      foreach ($this->entityVoteMap[$entity_type_id][$bundle] as $voting_type_id) {
        /** @var \Drupal\vote\Entity\VoteTypeInterface $voteType */
        $voteType = $this->voteTypes[$voting_type_id] ?: 0;

        if ($voteType && isset($results[$voting_type_id])) {
          $result = $results[$voting_type_id];
          $formatter = $voteType->getCondensedFormatter();
          $condensed = str_replace([
            '%result',
            '%raw',
            '%sum',
          ], [
            $result['result'],
            $result['raw'],
            $result['votesSum'],
          ], $formatter);

          $values[] = [
            'value' => $result['raw'],
            'type' => $voting_type_id,
            'icon' => $voteType->get('icon'),
            'condensed' => $condensed,
            'vote_type' => $this->spreadProps($voteType),
          ];
        }
      }
    }

    foreach ($values as $delta => $value) {
      $this->list[$delta] = $this->createItem($delta, $value);
    }
  }

  /**
   * Re-formate the vote_vote_type config entity as an array to export.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $voteType
   *   The vote type to re-formate.
   *
   * @return array
   *   The re-formatted config entity as array.
   */
  protected function spreadProps(ConfigEntityInterface $voteType) {
    $values = [];
    $props = ["id", "base_type", "label", "question", "allow_abstention", "value_definition",
      "labels", "rating_factor", "icon", "icon_family", "description", "condensed_format",
      "result_format", "color",
    ];
    foreach ($props as $prop) {
      $values[$prop] = $voteType->get($prop);
    }
    return $values;
  }

}
