<?php

namespace Drupal\vote;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheTagsInvalidator;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\votingapi\VoteResultFunctionManager;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * The interface to handle data operations with the Voting API .
 */
class VotingApiService {
  use StringTranslationTrait;

  /**
   * The cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The cache tag invalidator to handle data updates.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\votingapi\VoteResultFunctionManager definition.
   *
   * @var \Drupal\votingapi\VoteResultFunctionManager
   */
  protected $votingapiResult;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Collection of used VoteBaseType instances.
   *
   * @var \Drupal\vote\Entity\VoteTypeInterface[]
   */
  protected $voteTypes;

  /**
   * Collection of used VoteBaseType instances.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface[]
   */
  protected $entities;

  /**
   * The Drupal config factory.
   *
   * @var array|null
   */
  protected $voteConfig;

  /**
   * Voting api results collected in entity typed array.
   *
   * @var array
   */
  protected $rawResult = [];

  /**
   * User votes for entity.
   *
   * @var array
   */
  protected $userVotes;

  /**
   * The Vote Base Type Manager.
   *
   * @var VoteBaseTypeManager
   */
  protected $voteBaseTypeManager;

  /**
   * The Vote Storage.
   *
   * @var \Drupal\votingapi\VoteStorageInterface
   */
  protected $voteStorage;

  /**
   * Constructs a new VotingApiService object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    VoteResultFunctionManager $votingapi_result,
    MessengerInterface $messenger,
    AccountProxyInterface $current_user,
    ConfigFactoryInterface $config_factory,
    VoteBaseTypeManager $vote_base_type_manager,
    CacheBackendInterface $cache,
    CacheTagsInvalidator $cache_tags_invalidator
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->votingapiResult = $votingapi_result;
    $this->messenger = $messenger;
    $this->currentUser = $current_user;
    $this->voteBaseTypeManager = $vote_base_type_manager;
    $this->cache = $cache;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
    $this->voteConfig = $config_factory->get('vote.config')->get('entity_vote_map');
    $this->voteTypes = $entity_type_manager->getStorage('vote_vote_type')->loadMultiple();
    $this->voteStorage = $entity_type_manager->getStorage('vote');
  }

  /**
   * Main result function for a single target entity.
   *
   * @param string $entity_type_id
   *   Target entity type id.
   * @param int $entity_id
   *   Target entity id.
   *
   * @return array|null
   *   The Result of the voting combined with user data and single entity props.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function buildResult($entity_type_id, $entity_id) {
    $bundle = $this->getBundle($entity_type_id, $entity_id);
    if (isset($this->voteConfig[$entity_type_id][$bundle])) {
      $result = [];
      foreach ($this->voteConfig[$entity_type_id][$bundle] as $vote_type_id) {
        if ($vote_type = $this->getVoteType($vote_type_id)) {
          // Cacheable result.
          $raw_result = $this->getRawResult($entity_type_id, $entity_id, $vote_type_id);
          $result[$vote_type_id] = $this->voteBaseTypeManager
            ->getTypedResult($vote_type, $raw_result);
        }
      }
      return $result;
    }
    return NULL;
  }

  /**
   * Extend the voting result with entity props (created, changed, ...).
   *
   * @param array $results
   *   The result array to extend.
   * @param string $entity_type_id
   *   Target entity type id.
   * @param int $entity_id
   *   Target entity id.
   * @param array $props
   *   The props to extend the result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function extendResultEntityProps(array &$results, string $entity_type_id, int $entity_id, array $props = []) {
    foreach ($props as $prop) {
      $results[$prop] = [
        'abs' => $this->getTargetEntityProp($entity_type_id, $entity_id, $prop),
      ];
    }
  }

  /**
   * Calculates the total result of an entity as main sort criterion.
   *
   * @param array $results
   *   The result built from vote_vote_types.
   * @param int $precision
   *   The precision of the result.
   */
  public function extendResultTotal(array &$results, int $precision = 4) {
    $denom = 0;
    $num = 0;
    foreach ($results as $vote_type_id => $data) {
      $factor = ($vote_type = $this->getVoteType($vote_type_id))
        ? $vote_type->get('rating_factor') : 1;
      $num = $num + ($data['abs'] * $factor);
      $denom = $denom + $factor;
    }
    $results['ttl_res'] = [
      'abs' => ($denom > 0) ? round($num / $denom, $precision) : 0,
    ];
  }

  /**
   * Get bundle of given entity id.
   *
   * @param string $entity_type_id
   *   Target entity type id.
   * @param int $entity_id
   *   Target entity id.
   *
   * @return string|null
   *   Bundle of defined entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTargetEntity(string $entity_type_id, int $entity_id) {
    $slug = "{$entity_type_id}:{$entity_id}";
    if (!isset($this->entities[$slug])) {
      $this->entities[$slug] = $this->entityTypeManager->getStorage($entity_type_id)->load($entity_id);
    }
    return $this->entities[$slug];
  }

  /**
   * Get bundle of given entity id.
   *
   * @param string $entity_type_id
   *   Target entity type id.
   * @param int $entity_id
   *   Target entity id.
   *
   * @return string|null
   *   Bundle of defined entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getBundle(string $entity_type_id, int $entity_id) {
    if ($entity = $this->getTargetEntity($entity_type_id, $entity_id)) {
      return $entity->bundle();
    }
    return 'default';
  }

  /**
   * Get created date of given entity id.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param int $entity_id
   *   The entity id.
   * @param string $property
   *   Requested property id.
   *
   * @return string|null
   *   Created date of defined entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTargetEntityProp(string $entity_type_id, int $entity_id, string $property) {
    if ($entity = $this->getTargetEntity($entity_type_id, $entity_id)) {
      try {
        return $entity->get($property)->value;
      }
      catch (\Exception $exception) {
        return 0;
      }
    }
    return 0;
  }

  /**
   * Get all results of an entity from Voting API.
   *
   * @param string $entity_type_id
   *   Target entity type id.
   * @param int $entity_id
   *   Target entity id.
   * @param string|null $vote_type_id
   *   If vote_type_id is defined, returns a raw result dataset.
   *
   * @return array
   *   The raw result from
   */
  public function getRawResult(string $entity_type_id, int $entity_id, string $vote_type_id = NULL) {
    $slug = "{$entity_type_id}:{$entity_id}";
    if (!isset($this->rawResult[$slug])) {
      $this->rawResult[$slug] = $this->votingapiResult->getResults($entity_type_id, $entity_id);
    }
    if ($vote_type_id && isset($this->rawResult[$slug][$vote_type_id])) {
      return $this->rawResult[$slug][$vote_type_id];
    }
    return [];
  }

  /**
   * Get final formatted result with user specific data.
   *
   * @param string $entity_type_id
   *   The target entity type id.
   * @param int $entity_id
   *   The target entity id.
   * @param bool $omitOwnVotes
   *   Omit user specific data e.g. for static page caches.
   *
   * @return array
   *   Final formatted result with/without user specific data.
   */
  public function getResults(string $entity_type_id, int $entity_id, bool $omitOwnVotes = FALSE) {
    try {
      // Entity properties to extend the result for sorting reason.
      $extend_props = ['created', 'changed'];
      // @todo Extend the concept for more custom properties (e.g. status, ratified, promoted, ...).
      // Load results from cache if possible.
      $cache_id = "vote:result:{$entity_type_id}:{$entity_id}";
      if ($cached_results = $this->cache->get($cache_id)) {
        $results = $cached_results->data;
      }
      else {
        // Get results from voting api types.
        $results = $this->buildResult($entity_type_id, $entity_id);
        // Calculate total result and extend.
        $this->extendResultTotal($results);
        // Attach entity properties for sorting reason.
        $this->extendResultEntityProps($results, $entity_type_id, $entity_id, $extend_props);
        // Cache the result.
        $this->cache->set($cache_id, $results);
      }

      if (!$omitOwnVotes) {
        // Combine cached result data with cached user votes.
        foreach ($results as $vote_type_id => &$result) {
          if ($vote_type_id == 'ttl_res' || in_array($vote_type_id, $extend_props)) {
            continue;
          }
          $result['ownVote'] = $this->getUserVote($entity_type_id, $entity_id, $vote_type_id);
        }
      }
      return $results;
    }
    catch (\Exception $exception) {
      \Drupal::logger('vote')->error($exception->getMessage());
      return [];
    }
  }

  /**
   * Get the user vote for an entity.
   *
   * @param string $entity_type_id
   *   Target entity type id.
   * @param int $entity_id
   *   Target entity id.
   * @param string $vote_type_id
   *   Target entity id.
   *
   * @return mixed
   *   The user vote id (e.g. star3, yes).
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getUserVote(
    string $entity_type_id,
    int $entity_id,
    string $vote_type_id
  ) {
    $slug = "{$entity_type_id}:{$entity_id}";
    if ($uid = $this->currentUser->id()) {
      if (!$this->userVotes) {
        $this->initUserVotes($uid);
      }
      return $this->userVotes[$slug][$vote_type_id] ?? '';
    }
    else {
      return '';
    }
  }

  /**
   * Get the cached collection of all user votes.
   *
   * @param int $uid
   *   The user id.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function initUserVotes(int $uid) {
    $cache_id = "vote:own_votes:user:{$uid}";
    if ($caches_user_votes = $this->cache->get($cache_id)) {
      $this->userVotes = $caches_user_votes->data;
    }
    else {
      $vote_ids = $this->voteStorage->getUserVotes($uid);
      $this->userVotes = [];
      /** @var \Drupal\votingapi\VoteInterface $vote */
      foreach ($this->voteStorage->loadMultiple($vote_ids) as $vote) {
        $slug = "{$vote->getVotedEntityType()}:{$vote->getVotedEntityId()}";
        $vote_bundle = $vote->bundle();
        if ($vote_type = $this->voteTypes[$vote_bundle]) {
          $this->userVotes[$slug][$vote_bundle] = $this->voteBaseTypeManager
            ->getResultKeyFromValue($vote_type->get('base_type'), $vote->getValue());
        }
      }
      $this->cache->set($cache_id, $this->userVotes, CacheBackendInterface::CACHE_PERMANENT);
    }
  }

  /**
   * Get a vote_vote_type config entity from collection.
   *
   * @param string $vote_type_id
   *   The vote type ID.
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\vote\Entity\VoteTypeInterface|null
   *   Instance of VoteTypeInterface.
   */
  protected function getVoteType(string $vote_type_id) {
    return $this->voteTypes[$vote_type_id] ?: NULL;
  }

  /**
   * Refresh results and invalidate caches.
   *
   * @param string $entity_type_id
   *   Target entity type id.
   * @param int $entity_id
   *   Target entity id.
   * @param array $vote_type_ids
   *   Array of vote type ids.
   */
  public function refresh(string $entity_type_id, int $entity_id, array $vote_type_ids) {
    $slug = "$entity_type_id:$entity_id";
    foreach ($vote_type_ids as $vote_type_id) {
      $this->votingapiResult->recalculateResults($entity_type_id, $entity_id, $vote_type_id);
    }
    $this->cache->invalidate("vote:own_votes:user:{$this->currentUser->id()}");
    $this->cache->invalidate("vote:result:$slug");
  }

  /**
   * Record a vote.
   *
   * @param string $entity_type_id
   *   The entity type id e.g. node, media, ...
   * @param int|string $entity_id
   *   The entity id e.g. 12, 13, 14.
   * @param string $vote_type_id
   *   The vote type id e.g. helped, quality, ...
   * @param mixed $value
   *   The current users vote.
   *
   * @return array
   *   Result message.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function setVote($entity_type_id, $entity_id, $vote_type_id, $value) {
    $base_type = (isset($this->voteTypes[$vote_type_id]))
      ? $this->voteTypes[$vote_type_id]->get('base_type') : FALSE;
    if (!$base_type) {
      return [
        'result' => 'error',
        'check' => 'validation',
        'message' => $this->t('Sent value %val is not valid for voting type %type.', [
          '%type' => $vote_type_id,
        ]),
      ];
    }

    // Validate and get numeric value for current vote.
    $value = $this->validateVoteValue($base_type, $value);
    if (!(is_numeric($value) || ($value == "unvote"))) {
      return [
        'result' => 'error',
        'check' => 'validation',
        'message' => $this->t('Sent value %val is not valid for voting type %type.', [
          '%val' => $value,
          '%type' => $vote_type_id,
        ]),
      ];
    }

    // @todo $this->botDetector->checkIsBot(); see "Rate" module.
    $is_bot = FALSE;

    if (!$this->currentUser->hasPermission("vjsr add own votes")) {
      return [
        'result' => 'error',
        'check' => 'permission_add',
        'message' => $this->t('Permission to add votes is missing.'),
      ];
    }

    /** @var \Drupal\votingapi\VoteStorageInterface $vote_storage */
    $vote_storage = $this->entityTypeManager->getStorage('vote');
    $votes = $vote_storage->getUserVotes(
      $this->currentUser->id(),
      $vote_type_id,
      $entity_type_id,
      $entity_id
    );

    if (($value === "unvote") && !$this->currentUser->hasPermission("vjsr remove own votes")) {
      return [
        'result' => 'error',
        'check' => 'permission_delete',
        'message' => $this->t("You don't have permission to remove your votes."),
      ];
    }
    elseif (($value === "unvote") && count($votes)) {
      $vote = $vote_storage->load(reset($votes));
      $vote->delete();

      return [
        'result' => 'success',
        'check' => 'deleted',
        'message' => $this->t('Your vote has been deleted.'),
      ];
    }

    if (count($votes) && !$this->currentUser->hasPermission("vjsr edit own votes")) {
      return [
        'result' => 'error',
        'check' => 'permission_edit',
        'message' => $this->t('You have voted and not have permission to edit your votes.'),
      ];
    }

    if (count($votes) >= 1) {
      $vote = $vote_storage->load(reset($votes));
      $vote->setValue($value)
        ->save();

      return [
        'result' => 'success',
        'check' => 'updated',
        'message' => $this->t('Your vote has been updated.'),
      ];
    }

    /** @var \Drupal\votingapi\VoteTypeInterface $vote_type */
    $vote_type = $this->getVoteType($vote_type_id);

    /** @var \Drupal\votingapi\VoteInterface $vote */
    $vote_storage
      ->create(['type' => $vote_type_id])
      ->setVotedEntityId($entity_id)
      ->setVotedEntityType($entity_type_id)
      ->setValueType($vote_type->getValueType())
      ->setValue($value)
      ->save();

    return [
      'result' => 'success',
      'check' => 'created',
      'message' => $this->t('Your vote has been stored.'),
    ];
  }

  /**
   * Undo a vote.
   *
   * @param string $entity_type_id
   *   Entity type ID such as node.
   * @param int $entity_id
   *   Entity id of the entity type.
   * @param bool $show_messages
   *   If TRUE, standard Drupal message will be set.
   */
  public function undoVote($entity_type_id, $entity_id, $show_messages = TRUE) {
    $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity_id);
    // @todo $this->botDetector->checkIsBot();
    $is_bot = FALSE;

    if (!$is_bot && $this->currentUser->hasPermission('cast rate vote on ' . $entity_type_id . ' of ' . $entity->bundle())) {
      $vote_storage = $this->entityTypeManager->getStorage('vote');
      $vote_result = $vote_storage->getUserVotes(
        $this->currentUser->id(),
        NULL,
        $entity_type_id,
        $entity_id
      );

      // If a vote has been found, remove it.
      if (!empty($vote_result)) {
        $vote_ids = array_keys($vote_result);
        $vote_id = array_pop($vote_ids);
        $vote = $vote_storage->load($vote_id);
        if ($vote) {
          $vote->delete();
        }
      }
    }
  }

  /**
   * Validates whether a value is allowed for a given vote type.
   *
   * @param string $base_type
   *   Vote type id.
   * @param string $value
   *   The vote value.
   *
   * @return int|null
   *   Returns numeric value of result function if is allowed.
   *   Returns NULL if base_type or result function not exists.
   */
  public function validateVoteValue(string $base_type, string $value) {
    if ($def = $this->voteBaseTypeManager->getDefinition($base_type)) {
      if (in_array($value, array_keys($def['value_definition']))) {
        return $def['value_definition'][$value];
      }
      elseif ($value === "") {
        return "unvote";
      }
    }
    return NULL;
  }

  /**
   * Check if voting type has already votes.
   *
   * @param string $vote_type_id
   *   The vote type id.
   *
   * @return bool
   *   Returns true if voteType has votes.
   */
  public function hasVoteTypeVotes(string $vote_type_id) {
    $query = \Drupal::entityQuery('vote')
      ->accessCheck(FALSE)
      ->condition('type', $vote_type_id)
      ->execute();

    return (count($query) >= 1);
  }

}
