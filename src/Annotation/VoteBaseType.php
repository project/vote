<?php

namespace Drupal\vote\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a VoteBaseType annotation object.
 *
 * @ingroup vote_api
 *
 * @Annotation
 */
class VoteBaseType extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The administrative label of the block.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $admin_label = '';

  /**
   * The description of base vote type.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description = '';

  /**
   * Assoc array of options definitions and their result plugin id.
   *
   * Key is.result plugin id and value the (redundant) numeric value.
   *
   * @var array
   */
  public $value_definition = [];

  /**
   * Assoc array where key is result plugin id and value is label.
   *
   * @var array
   */
  public $labels = [];

  /**
   * String formatter for default condensed result output.
   *
   * @var string
   */
  public $condensed_format = '';

  /**
   * String formatter for default longer result output.
   *
   * @var string
   */
  public $result_format = '';

}
