<?php

namespace Drupal\vote\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Vote type entities.
 */
interface VoteTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
