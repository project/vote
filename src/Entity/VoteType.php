<?php

namespace Drupal\vote\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\vote\TypedData\Options\VoteResultQuality as VRQ;

/**
 * Defines the Vote type entity.
 *
 * @ConfigEntityType(
 *   id = "vote_vote_type",
 *   label = @Translation("VOTE! Type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\vote\VoteTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\vote\Form\VoteTypeForm",
 *       "edit" = "Drupal\vote\Form\VoteTypeForm",
 *       "delete" = "Drupal\vote\Form\VoteTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\vote\VoteTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "vote_vote_type",
 *   admin_permission = "administer vote types",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "base_type",
 *     "label",
 *     "question",
 *     "allow_abstention",
 *     "value_definition",
 *     "labels",
 *     "rating_factor",
 *     "icon",
 *     "icon_family",
 *     "description",
 *     "condensed_format",
 *     "result_format",
 *     "color",
 *     "quality_neutral_max",
 *     "quality_neutral_min"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/services/vote/types/{vote_vote_type}",
 *     "add-form" = "/admin/config/services/vote/types/add",
 *     "edit-form" = "/admin/config/services/vote/types/{vote_vote_type}/edit",
 *     "delete-form" = "/admin/config/services/vote/types/{vote_vote_type}/delete",
 *     "collection" = "/admin/config/services/vote"
 *   }
 * )
 */
class VoteType extends ConfigEntityBase implements VoteTypeInterface {

  /**
   * The Vote type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Vote type label.
   *
   * @var string
   */
  protected $label;

  /**
   * Get the value type of voting.
   */
  public function getValueType() {
    if ($vote_type = $this->entityTypeManager()->getStorage('vote_type')->load($this->id)) {
      return $vote_type->getValueType();
    }
    else {
      return 'points';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    $base_type = $this->get('base_type');

  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Create or update the corresponding voting type in the Voting API.
    $voting_api_entity_name = "votingapi.vote_type.{$this->id()}";
    $voting_api_entity = \Drupal::configFactory()->getEditable($voting_api_entity_name);
    if ($voting_api_entity->isNew()) {
      $voting_api_entity->set('id', $this->id());

      $uuid = \Drupal::service('uuid')->generate();
      $voting_api_entity->set('uuid', $uuid);

      $langcode = $this->languageManager()->getDefaultLanguage()->getId();
      $voting_api_entity->set('langcode', $langcode);

      $voting_api_entity->set('status', TRUE);
    }

    $voting_api_entity->set('label', $this->get('label'));
    $voting_api_entity->set('value_type', 'points');
    $voting_api_entity->set('description', 'Generated automated by Vote module. Do not edit directly or delete.');
    $voting_api_entity->set('dependencies', ['enforced' => ['config' => ["vote.vote_vote_type.{$this->id()}"]]]);
    $voting_api_entity->save();
  }

  /**
   * Returns the result formatter string.
   *
   * @return string|null
   *   Result formatter string.
   */
  public function getResultFormatter() {
    return $this->get('result_format');
  }

  /**
   * Returns the condensed formatter string.
   *
   * @return string|null
   *   Condensed formatter string.
   */
  public function getCondensedFormatter() {
    return $this->get('condensed_format');
  }

  /**
   * Returns the quality type (good, neutral, bad).
   *
   * @param mixed $value
   *   The value to compare with config.
   *
   * @return string
   *   Quality type.
   */
  public function getQuality(mixed $value): string {
    if ($value > ($this->get('quality_neutral_max') ?? 10)) {
      return VRQ::QUALITY_GOOD;
    }
    elseif ($value < ($this->get('quality_neutral_min') ?? -10)) {
      return VRQ::QUALITY_BAD;
    }
    return VRQ::QUALITY_NEUTRAL;
  }

}
