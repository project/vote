<?php

namespace Drupal\vote\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the vote module.
 */
class VoteVoteControllerTest extends WebTestBase {

  /**
   * Drupal\vote\VotingApiService definition.
   *
   * @var \Drupal\vote\VotingApiService
   */
  protected $voteVotingapi;

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "vote VoteVoteController's controller functionality",
      'description' => 'Test Unit for module vote and controller VoteVoteController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests vote functionality.
   */
  public function testVoteVoteController() {
    // Check that the basic functions of module vote.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
